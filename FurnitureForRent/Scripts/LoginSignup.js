﻿function lightbox_close() {
    $('#light').css('display', 'none');
    $('#fade').css('display', 'none');
}
function lightbox_open() {
    window.scrollTo(0, 0);
    $('#light').css('display', 'block');
    $('#fade').css('display', 'block');
}
window.document.onkeydown = function (e) {
    if (!e) {
        e = event;
    }
    if (e.keyCode == 27) {
        lightbox_close();
    }
}
$("#furnitureCode").focusin(function () {
    $(".rm_banner").css("-webkit-filter", "grayscale(100%)").css("filter", "grayscale(100%)")
});
$("#furnitureCode").focusout(function () {
    $(".rm_banner").css("-webkit-filter", "grayscale(0%)").css("filter", "grayscale(0%)")
});
$("#furnitureCode").click(function (e) {
    if (e.keyCode === 13) {
        $("#furnitureCode").attr("type", "submit");
    }
});
$("#furniture").change(function (e) {
    this.form.submit();
});
$(".form-control").click(function () { $(".form-control").css("background-color", "transparent") });

$(document).ready(function () {
    $('#list').click(function (event) { event.preventDefault(); $('#products .item').addClass('list-group-item'); });
    $('#grid').click(function (event) { event.preventDefault(); $('#products .item').removeClass('list-group-item'); $('#products .item').addClass('grid-group-item'); });
});