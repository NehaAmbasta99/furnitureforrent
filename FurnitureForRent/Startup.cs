﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FurnitureForRent.Startup))]

namespace FurnitureForRent
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}