﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: HomeSearchViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining Furniture Id, Furniture code,rent per month,category,material,color,dimension,specification,search code,
//              tenure,furniturelist
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.Web.Mvc;

namespace FurnitureForRent.Models
{
    public class HomeSearchViewModel
    {
        public int FurnitureId { get; set; }
        public string FurnitureCode { get; set; }
        public int RentPerMonth { get; set; }
        public string Category { get; set; }
        public string Material { get; set; }
        public string Color { get; set; }
        public string Dimension { get; set; }
        public string Specification { get; set; }
        public string SearchCode { get; set; }
        public int Tenure { get; set; }
        public SelectList FurnitureList { get; set; }
    }
}