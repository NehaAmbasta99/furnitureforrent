﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: PlaceOrderViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining Furniture Id,Order Id,User Id,Quantity,Tenure,Address Id,Rental due,adjustment,cart id
//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace FurnitureForRent.Models
{
    public class PlaceOrderViewModel
    {
        public int FurnitureId { get; set; }
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public int Quantity { get; set; }
        public int Tenure { get; set; }
        public int AddressId { get; set; }
        public int RentalDue { get; set; }
        public int Adjustment { get; set; }
        public int CartId { get; set; }
    }
}