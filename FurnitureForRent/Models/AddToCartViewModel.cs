﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AddToCartViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining Furniture Id, User Id and Tenure
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.ComponentModel.DataAnnotations;

namespace FurnitureForRent.Models
{
    public class AddToCartViewModel
    {
        public int FurnitureId { get; set; }
        public int UserId { get; set; }

        [Required]
        public int Tenure { get; set; }
    }
}