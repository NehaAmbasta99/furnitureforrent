﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: SignInSignUpViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining full name,mobile number,password,username , email id and hashed password for sign in and sign up
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.ComponentModel.DataAnnotations;

namespace FurnitureForRent.Models
{
    public class SignInSignUpViewModel
    {
        public SignUpViewModel SignUpViewModel { get; set; }
        public SignInViewModel SignInViewModel { get; set; }
    }

    public class SignUpViewModel
    {
        [Required]
        [Display(Name = "fullname")]
        public string FullName { get; set; }

        [Phone]
        [Required]
        [Display(Name = "phone")]
        public string MobileNumber { get; set; }

        [Display(Name = "password")]
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        public string Hash { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [Required]
        [Display(Name = "email")]
        public string EmailId { get; set; }
    }

    public class SignInViewModel
    {
        public string EmailId { get; set; }

        public string MobileNumber { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Email/Mobile Number")]
        public string UserName { get; set; }

        public string Hash { get; set; }
    }
}