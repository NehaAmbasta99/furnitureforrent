﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: HomeController.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining action methods for homepage,Sign in,Sign up,Display Furniture By dropdown or text,Display All Furniture,Error,
//              Unauthorized,Sign out and Add to cart
////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.BusinessLayer;
using FurnitureForRent.Enum;
using FurnitureForRent.Models;
using System;
using System.Web.Mvc;

namespace FurnitureForRent.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            try
            {
                HomepageBL home = new HomepageBL();
                return View(home.GetFurnitureList());
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult DisplayFurnitureById(HomeSearchViewModel furniture)
        {
            try
            {
                HomepageBL home = new HomepageBL();
                return View(home.GetFurnitureDetails(furniture));
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult DisplayAllFurniture()
        {
            try
            {
                HomepageBL home = new HomepageBL();
                return View(home.DisplayAllFurniture());
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult SignIn(SignInSignUpViewModel signInSignUpViewModel)
        {
            var login = signInSignUpViewModel.SignInViewModel;
            try
            {
                if (ModelState.IsValid)
                {
                    LoginBL user = new LoginBL();
                    var userId = user.ValidateUser(login);
                    if (userId > 0)
                    {
                        Session["userId"] = userId;
                        var roleId = user.GetUserRole(userId);
                        Session["roleId"] = roleId;
                        if (roleId == ((int)Role.Admin))
                        {
                            return RedirectToAction("AdminProfile", "Admin", new { area = "Admin" });
                        }
                        else
                        {
                            return RedirectToAction("UserProfile", "User", new { area = "User" });
                        }
                    }
                    else
                    {
                        ViewBag.Message = Message.IncorrectData;
                        return View("SignUp");
                    }
                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return View("SignUp");
                }
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult SignUp()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult SignUp(SignInSignUpViewModel signInSignUpViewModel)
        {
            var registerViewModel = signInSignUpViewModel.SignUpViewModel;

            try
            {
                RegisterBL user = new RegisterBL();
                if (ModelState.IsValid)
                {
                    int userExists = user.UserRegister(registerViewModel);
                    if (userExists == -1)
                    {
                        ModelState.Clear();
                        ViewBag.Message = Message.DataAlreadyAvailable;
                        return View(); ;
                    }
                    else
                    {
                        ModelState.Clear();
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return View();
                    }
                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return View();
                }
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return View();
        }

        [AuthorizeUserAccessLevel(Role.Admin,Role.User)]
        public ActionResult SignOut()
        {
            Session.Remove("userId");
            return Redirect("SignUp");
        }

        //[AuthorizeUserAccessLevel(Role.User)]

        public ActionResult AddToCart(int id)
        {
            UserBL user = new UserBL();
            int userId;
            if(Session["userId"]!=null)
            {
                int.TryParse((Session["userId"]).ToString(), out userId);
                AddToCartViewModel addToCartViewModel = new AddToCartViewModel();
                addToCartViewModel.FurnitureId = id;
                addToCartViewModel.Tenure = 4;
                addToCartViewModel.UserId = userId;
                user.AddToCart(addToCartViewModel);
                ViewBag.Message = Message.AddedToCart;
                return RedirectToAction("UserProfile", "User");
            }
            else
            {
                return RedirectToAction("Unauthorized");
            }
            
        }
    }
}