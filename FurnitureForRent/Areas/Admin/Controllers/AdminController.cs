﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AdminController.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining action methods for displaying and editing admin profile,Change password,displaying all Users,adding a admin
//              Add furniture,Display furniture,Update furniture,Delete furniture,
//              Activate user,deactivate user,update user,
//              display address,add address,update address
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.Areas.Admin.Models;
using FurnitureForRent.BusinessLayer;
using FurnitureForRent.Enum;
using FurnitureForRent.Models;
using System;
using System.Web.Mvc;

namespace FurnitureForRent.Areas.Admin.Controllers
{
    [AuthorizeUserAccessLevel(Role.Admin)]
    public class AdminController : Controller
    {
        // GET: Admin/Admin        
        public ActionResult AdminProfile()
        {
            try
            {
                AdminBL admin = new AdminBL();
                int userId;
                if(Session["userId"]!=null)
                {
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    return View(admin.ShowProfile(userId));
                }                
                else
                {
                    return Redirect("~/Home/Unauthorized");
                }
                
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserAccessLevel(Role.Admin)]
        public ActionResult AdminProfile(AdminProfileViewModel adminProfileViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    if (Session["userId"] != null)
                    {
                        int.TryParse((Session["userId"]).ToString(), out userId);
                        AdminBL admin = new AdminBL();
                        adminProfileViewModel.UserId = userId;
                        admin.EditProfile(adminProfileViewModel);
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return View(admin.ShowProfile(userId));
                    }
                    else
                    {
                        return Redirect("~/Home/Unauthorized");
                    }

                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
            
        }

        [AuthorizeUserAccessLevel(Role.Admin)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserAccessLevel(Role.Admin)]
        public ActionResult ChangePassword(FurnitureForRent.Areas.Admin.Models.ChangePasswordViewModel changePasswordViewModel)
        {
            try
            {
                ChangePasswordBL user = new ChangePasswordBL();
                if (ModelState.IsValid)
                {
                    int userId;
                    if(Session["userId"]!=null)
                    {
                        int.TryParse((Session["userId"]).ToString(), out userId);
                        changePasswordViewModel.UserId = userId;
                        int userExists = user.ChangePassword(changePasswordViewModel);
                        if (userExists == -1)
                        {
                            ModelState.Clear();
                            ViewBag.Message = Message.IncorrectData;
                            return View(); ;
                        }
                        else
                        {
                            ModelState.Clear();
                            ViewBag.Message = Message.DataSavedSuccessfully;
                            return RedirectToAction("AdminProfile");
                        }
                    }
                    else
                    {
                        ViewBag.Message = Message.PleaseSignIn;
                        return Redirect("~/Home/SignUp");
                    }
                    
                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        [AuthorizeUserAccessLevel(Role.Admin)]
        public ActionResult Users()
        {
            try
            {
                AdminBL admin = new AdminBL();
                int userId;
                if(Session["userId"]!=null)
                {
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    return View(admin.ShowAllUsers());
                }
                else
                {
                    return Redirect("~/Home/Unauthorized");
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        
        public ActionResult AddFurniture()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddFurniture(AddFurnitureViewModel addfurnitureviewmodel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AdminBL admin = new AdminBL();
                    admin.AddFurniture(addfurnitureviewmodel);
                    ViewBag.Message = Message.DataSavedSuccessfully;
                    ModelState.Clear();
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
            return View();
        }

      
        public ActionResult DisplayFurniture()
        {
            try
            {
                AdminBL admin = new AdminBL();
                return View(admin.DisplayAllFurniture());
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

  
        public ActionResult UpdateFurniture(int id)
        {
            try
            {
                AdminBL admin = new AdminBL();
                return View(admin.GetFurnitureDetails(id));
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        [HttpPost]
        public ActionResult UpdateFurniture(AddFurnitureViewModel addFurnitureViewModel)
        {
            if (ModelState.IsValid)
            {
                AdminBL admin = new AdminBL();
                admin.UpdateFurniture(addFurnitureViewModel);
                ViewBag.Message = Message.DataSavedSuccessfully;
                ModelState.Clear();
                return View();
            }
            else
            {
                ViewBag.Message = Message.EnterDataInAllFields;
                ModelState.Clear();
                return View();
            }
        }

       
        
        public ActionResult DeleteFurniture(int id)
        {
            AdminBL admin = new AdminBL();
            admin.DeleteFurniture(id);
            return RedirectToAction("DisplayFurniture");
        }

        
        public ActionResult ActivateUser(int id)
        {
            AdminBL admin = new AdminBL();
            admin.ActivateUser(id);
            return RedirectToAction("Users");
        }

  
        public ActionResult AddAdmin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAdmin(SignInSignUpViewModel signInSignUpViewModel)
        {
            var registerViewModel = signInSignUpViewModel.SignUpViewModel;

            try
            {
                RegisterBL user = new RegisterBL();
                if (ModelState.IsValid)
                {
                    int userExists = user.AdminRegister(registerViewModel);
                    if (userExists == -1)
                    {
                        ModelState.Clear();
                        ViewBag.Message = Message.DataAlreadyAvailable;
                        return View(); ;
                    }
                    else
                    {
                        ModelState.Clear();
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return View();
                    }
                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

       

       
        public ActionResult DeactivateUser(int id)
        {
            AdminBL admin = new AdminBL();
            admin.DeactivateUser(id);
            return RedirectToAction("Users");
        }

       
        public ActionResult OrdersAndPayments()
        {
            AdminBL admin = new AdminBL();
            return View(admin.DisplayOrdersAndPayments());
        }
        public ActionResult UpdateUser(int id)
        {
            try
            {
                AdminBL admin = new AdminBL();
                return View(admin.ShowProfile(id));
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

       
        public ActionResult ShowAddress()
        {
            int userId;
            if (Session["userId"] != null)
            {
                int.TryParse((Session["userId"]).ToString(), out userId);
                AdminBL user = new AdminBL();
                return View(user.ShowAllAddress(userId));
            }
            else
            {
                return Redirect("~/Home/Unauthorized");
            }
        }

       
        public ActionResult AddAddress()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAddress(AddAddressViewModel addAddressViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    if (Session["userId"] != null)
                    {
                        int.TryParse((Session["userId"]).ToString(), out userId);
                        AdminBL user = new AdminBL();
                        addAddressViewModel.UserId = userId;
                        user.AddAddress(addAddressViewModel);
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return View(user.ShowAllAddress(userId));
                    }
                    else
                    {
                        return Redirect("~/Home/Unauthorized");
                    }
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUser(AdminProfileViewModel user)
        {
            if (ModelState.IsValid)
            {
                AdminBL admin = new AdminBL();
                admin.EditProfile(user);
                ViewBag.Message = Message.DataSavedSuccessfully;
                ModelState.Clear();
                return View();
            }
            else
            {
                ViewBag.Message = Message.EnterDataInAllFields;
                ModelState.Clear();
                return View();
            }
        }

       
        public ActionResult EditAddress(int id)
        {
            try
            {
                AdminBL user = new AdminBL();
                return View(user.ShowAddressById(id));
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAddress(AddAddressViewModel addAddressViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    if (Session["userId"] != null)
                    {
                        int.TryParse((Session["userId"]).ToString(), out userId);
                        AdminBL user = new AdminBL();
                        addAddressViewModel.UserId = userId;
                        user.EditAddress(addAddressViewModel);
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return RedirectToAction("ShowAddress");
                    }
                    else
                    {
                        return Redirect("~/Home/Unauthorized");
                    }
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
            return View();
        }

    }
}