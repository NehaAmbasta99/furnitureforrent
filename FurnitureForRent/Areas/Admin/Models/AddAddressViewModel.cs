﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AddAddressViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining Address Id,Flat or House number,society name,address1,address2,alternative mobile number,area,landmark,pincode,
//              CityId,UserId,CityName
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurnitureForRent.Areas.Admin.Models
{
    public class AddAddressViewModel
    {
        public int AddressId { get; set; }
        public string FlatOrHouseNumber { get; set; }
        public string SocietyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string AlternativeMobileNumber { get; set; }
        public string Area { get; set; }
        public string Landmark { get; set; }
        public string Pincode { get; set; }
        public int CityId { get; set; }
        public int UserId { get; set; }
        public string CityName { get; set; }

    }
}