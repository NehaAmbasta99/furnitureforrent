﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: DisplayOrdersAndPayments.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining order id,tenure,quantity,rental due,late fee,adjustment,status,furniture id,furniture code,order date
//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace FurnitureForRent.Areas.Admin.Models
{
    public class DisplayOrdersAndPayments
    {
        public int OrderId { get; set; }
        public int Tenure { get; set; }
        public int Quantity { get; set; }
        public int RentalDue { get; set; }
        public int LateFee { get; set; }
        public int Adjustment { get; set; }
        public string Status { get; set; }
        public int FurnitureId { get; set; }
        public string FurnitureCode { get; set; }
        public string OrderDate { get; set; }
    }
}