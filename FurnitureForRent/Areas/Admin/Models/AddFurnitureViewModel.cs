﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AddFurnitureViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining furniture code,refundable deposit,material,color,dimension,specification,rent per month,Image,category,furniture id
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.ComponentModel.DataAnnotations;

namespace FurnitureForRent.Areas.Admin.Models
{
    public class AddFurnitureViewModel
    {
        [Required]
        public string FurnitureCode { get; set; }

        [Required]
        public string RefundableDeposit { get; set; }

        public string Material { get; set; }

        public string Color { get; set; }

        public string Dimension { get; set; }

        public string Specification { get; set; }

        [Required]
        public string RentPerMonth { get; set; }

        public string Image { get; set; }

        [Required]
        public string Category { get; set; }

        public string FurnitureId { get; set; }
    }
}