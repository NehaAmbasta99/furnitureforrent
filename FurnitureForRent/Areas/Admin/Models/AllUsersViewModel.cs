﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AllUsersViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining user id,full name,emailid,status,mobile number
//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace FurnitureForRent.Areas.Admin.Models
{
    public class AllUsersViewModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string EmailId { get; set; }
        public string Status { get; set; }
        public string MobileNumber { get; set; }
    }
}