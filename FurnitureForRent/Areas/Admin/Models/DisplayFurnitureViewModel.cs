﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: DisplayFurnitureViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining furniture id,furniture code,rent per month,category of the furniture
//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace FurnitureForRent.Areas.Admin.Models
{
    public class DisplayFurnitureViewModel
    {
        public int FurnitureId { get; set; }

        public string FurnitureCode { get; set; }

        //public string RefundableDeposit { get; set; }

        //public string Material { get; set; }

        //public string Color { get; set; }

        //public string Dimension { get; set; }

        //public string Specification { get; set; }

        public string RentPerMonth { get; set; }

        //public string Image { get; set; }

        public string Category { get; set; }
    }
}