﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: UserProfileViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining user id,full name,email id,password,mobile number
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.ComponentModel.DataAnnotations;
namespace FurnitureForRent.Areas.User.Models
{
    public class UserProfileViewModel
    {
        public int UserId { get; set; }
        [Required]
        public string FullName { get; set; }

        [Required]
        public string EmailId { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string MobileNumber { get; set; }
    }
}