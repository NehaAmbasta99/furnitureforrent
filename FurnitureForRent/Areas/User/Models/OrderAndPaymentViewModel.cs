﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: OrderAndPaymentViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining order id,tenure,quantity,rental due,late fee,adjustment,status,furniture id,furniture code,rent per month,
//              refundable deposit,cart id,order date
//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace FurnitureForRent.Areas.User.Models
{
    public class OrderAndPaymentViewModel
    {
        public int OrderId { get; set; }
        public int Tenure { get; set; }
        public int Quantity { get; set; }
        public int RentalDue { get; set; }
        public int LateFee { get; set; }
        public int Adjustment { get; set; }
        public string Status { get; set; }
        public int FurnitureId { get; set; }
        public string FurnitureCode { get; set; }
        public int RentPerMonth { get; set; }
        public int RefundableDeposit { get; set; }
        public int CartId { get; set; }
        public string OrderDate { get; set; }
    }
}