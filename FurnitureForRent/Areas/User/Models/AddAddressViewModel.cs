﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AddAddressViewModel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Model for defining address properties like address id,flat or house number,society name,address1,addresss2,alternative mobile number
//              area,landmark,pincode,cityid,user id,city name
//////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurnitureForRent.Areas.User.Models
{
    public class AddAddressViewModel
    {
        public int AddressId { get; set; }
        public string FlatOrHouseNumber { get; set; }
        public string SocietyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string AlternativeMobileNumber { get; set; }
        public string Area { get; set; }
        public string Landmark { get; set; }
        public string Pincode { get; set; }
        public int CityId { get; set; }
        public int UserId { get; set; }
        public string CityName { get; set; }

    }
}