﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: UserController.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining action methods for displaying and editing user profile,Change password,
//              display address,add address,update address,
//              Orders And Payments,Furniture Cart,Cancel Order
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.Areas.User.Models;
using FurnitureForRent.BusinessLayer;
using FurnitureForRent.Enum;
using FurnitureForRent.Models;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace FurnitureForRent.Areas.User.Controllers
{
    [AuthorizeUserAccessLevel(Role.User)]
    public class UserController : Controller
    {
        // GET: User/User
        
        public ActionResult UserProfile()
        {
            try
            {
                UserBL user = new UserBL();
                int userId;
                if(Session["userId"]!=null)
                {
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    return View(user.ShowProfile(userId));
                }
                else
                {
                    return RedirectToRoute("~/Home/Unauthorized");
                }
               
            }
            catch (Exception)
            {
                return RedirectToRoute("~/Home/Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserProfile(UserProfileViewModel userProfileViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    if(Session["userId"]!=null)
                    {
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    UserBL user = new UserBL();
                    userProfileViewModel.UserId = userId;
                    user.EditProfile(userProfileViewModel);
                    ViewBag.Message = Message.DataSavedSuccessfully;
                    return View(user.ShowProfile(userId));
                    }
                   
                }
                else
                {
                    return Redirect("~/Home/Unauthorized");
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
            return View();
        }

        
        public ActionResult AddAddress()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAddress(AddAddressViewModel addAddressViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    if (Session["userId"] != null)
                    {
                        int.TryParse((Session["userId"]).ToString(), out userId);
                        UserBL user = new UserBL();
                        addAddressViewModel.UserId = userId;
                        user.AddAddress(addAddressViewModel);
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return View(user.ShowAllAddress(userId));
                    }
                    else
                    {
                        return Redirect("~/Home/Unauthorized");
                    }
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
            return View();
        }

        
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(FurnitureForRent.Areas.Admin.Models.ChangePasswordViewModel changePasswordViewModel)
        {
            try
            {
                ChangePasswordBL user = new ChangePasswordBL();
                if (ModelState.IsValid)
                {
                    int userId;
                    if (Session["userId"]!=null)
                    {
                        int.TryParse((Session["userId"]).ToString(), out userId);
                        changePasswordViewModel.UserId = userId;
                        int userExists = user.ChangePassword(changePasswordViewModel);
                        if (userExists == -1)
                        {
                            ModelState.Clear();
                            ViewBag.Message = Message.IncorrectData;
                            return View();
                        }
                        else
                        {
                            ModelState.Clear();
                            ViewBag.Message = Message.DataSavedSuccessfully;
                            string sendEmail = ConfigurationManager.AppSettings["SendEmail"];
                            if (string.Compare(sendEmail, "True", true) == 0)
                            {
                                MailBL.SendEmail(Message.PasswordChanged, Message.Notification);
                            }
                            return RedirectToAction("UserProfile");
                        }
                    }
                    else
                    {
                        return Redirect("~/Home/Unauthorized");
                    }
                    
                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        
        public ActionResult ShowAddress()
        {
            int userId;
            if (Session["userId"]!=null)
            {
                int.TryParse((Session["userId"]).ToString(), out userId);
                UserBL user = new UserBL();
                return View(user.ShowAllAddress(userId));
            }
            else
            {
                return Redirect("~/Home/Unauthorized");
            }
            
        }


        
        public ActionResult OrdersAndPayments()
        {
            int userId;
            if (Session["userId"] != null)
            {
                int.TryParse((Session["userId"]).ToString(), out userId);
                UserBL user = new UserBL();
                return View(user.DisplayOrdersAndPayments(userId));
            }
            else
            {
                return Redirect("~/Home/Unauthorized");
            }
        }

        
        public ActionResult FurnitureCart()
        {
            int userId;
            if(Session["userId"]!=null)
            {
                int.TryParse((Session["userId"]).ToString(), out userId);
                UserBL user = new UserBL();
                return View(user.DisplayCart(userId));
            }
            else
            {
                return Redirect("~/Home/Unauthorized");
            }
            
        }

        [HttpPost]
        public ActionResult FurnitureCart(PlaceOrderViewModel placeOrderViewModel)
        {
            UserBL user = new UserBL();
            int userId;
            if (Session["userId"] != null)
            {
                int.TryParse((Session["userId"]).ToString(), out userId);
                placeOrderViewModel.UserId = userId;
                user.PlaceOrder(placeOrderViewModel);
                string sendEmail = ConfigurationManager.AppSettings["SendEmail"];
                if (string.Compare(sendEmail, "True", true) == 0)
                {
                    MailBL.SendEmail(Message.OrderPlaced, Message.Confirmation);
                }
                return View();
            }
            else
            {
                return Redirect("~/Home/Unauthorized");
            }
        }

        
                
        public ActionResult CancelOrder(int id)
        {
            int userId;
            if (Session["userId"] != null)
            {

                int.TryParse((Session["userId"]).ToString(), out userId);
                UserBL user = new UserBL();
                PlaceOrderViewModel placeOrderViewModel = new PlaceOrderViewModel();
                placeOrderViewModel.UserId = userId;
                placeOrderViewModel.OrderId = id;
                user.CancelOrder(placeOrderViewModel);
                string sendEmail = ConfigurationManager.AppSettings["SendEmail"];
                if (string.Compare(sendEmail, "True", true) == 0)
                {
                    MailBL.SendEmail(Message.OrderCancelled+""+ "Order No.=" + placeOrderViewModel.OrderId, Message.Cancellation);
                }
                return RedirectToAction("OrdersAndPayMents");
            }
            else
            {
                return Redirect("~/Home/Unauthorized");
            }
        }

        
        public ActionResult EditAddress(int id)
        {
            try
            {
                UserBL user = new UserBL();
                return View(user.ShowAddressById(id));
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAddress(AddAddressViewModel addAddressViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    if (Session["userId"] != null)
                    {

                        int.TryParse((Session["userId"]).ToString(), out userId);
                        UserBL user = new UserBL();
                        addAddressViewModel.UserId = userId;
                        user.EditAddress(addAddressViewModel);
                        ViewBag.Message = Message.DataSavedSuccessfully;
                        return RedirectToAction("ShowAddress");
                    }
                    else
                    {
                        return Redirect("~/Home/Unauthorized");
                    }
                }
                else
                {
                    ViewBag.Message = Message.EnterDataInAllFields;
                    return RedirectToAction("EditAddress");
                }
            }
            catch (Exception)
            {
                return Redirect("~/Home/Error");
            }         
        }

    }
}