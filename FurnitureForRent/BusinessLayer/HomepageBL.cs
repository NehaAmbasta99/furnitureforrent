﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: HomepageBL.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining business layer methods for Getting Furniture list for dropdown on homepage
//              Get Furniture Details , display All Furniture
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.DataAccessLayer;
using FurnitureForRent.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FurnitureForRent.BusinessLayer
{
    public class HomepageBL
    {
        public HomeSearchViewModel GetFurnitureList()
        {
            HomeSearchViewModel homeSearchViewModel = new HomeSearchViewModel();
            HomepageDA home = new HomepageDA();
            var list = home.GetFurnitureList();
            foreach (var item in list)
            {
                item.FurnitureCode = item.FurnitureCode + " , " + item.Category + " , " + item.Color + " , ₹ " + item.RentPerMonth;
            }
            homeSearchViewModel.FurnitureList = new SelectList(list, "FurnitureId", "FurnitureCode");
            return homeSearchViewModel;
        }

        public List<HomeSearchViewModel> GetFurnitureDetails(HomeSearchViewModel search)
        {
            HomeSearchViewModel homeSearchViewModel = new HomeSearchViewModel();
            HomepageDA home = new HomepageDA();
            List<HomeSearchViewModel> list = home.GetFurnitureDetails(search);
            return list;
        }

        public List<HomeSearchViewModel> DisplayAllFurniture()
        {
            HomeSearchViewModel homeSearchViewModel = new HomeSearchViewModel();
            HomepageDA home = new HomepageDA();
            List<HomeSearchViewModel> list = home.DisplayAllFurniture();
            return list;
        }
    }
}