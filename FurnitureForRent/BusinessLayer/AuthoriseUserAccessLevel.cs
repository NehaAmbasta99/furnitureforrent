﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: AuthoriseUserAccessLevel.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Authorization for admin and user
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.Enum;
using System.Web;
using System.Web.Mvc;

namespace FurnitureForRent.BusinessLayer
{
    public class AuthorizeUserAccessLevel : AuthorizeAttribute
    {
        private readonly Role[] acceptedRoles;

        public AuthorizeUserAccessLevel(params Role[] roles)
        {
            acceptedRoles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var currentUser = "";
            if (httpContext.Session["roleId"] != null)
            {
                var currentRole = httpContext.Session["roleId"].ToString();
                if (currentRole != null)
                {
                    if (currentRole.Equals("1"))
                    {
                        currentUser = "Admin";
                    }
                    else
                    {
                        currentUser = "User";
                    }
                    for (int i = 0; i < acceptedRoles.Length; i++)
                    {
                        if (currentUser.Equals(acceptedRoles[i].ToString()))
                        {
                            return true;
                        }
                       
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            filterContext.Result = new RedirectResult("~/Home/Unauthorized");
        }
    }
}