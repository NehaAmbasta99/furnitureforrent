﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: UserBL.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining business layer methods for displaying profile,adding address,displaying a address by its id,displaying  all address,
//              edit profile,edit address,displaying orders and payments,placing an order,cancel an order,add to cart,display cart
//////////////////////////////////////////////////////////////////////////////////////////////////////////

using FurnitureForRent.Areas.User.Models;
using FurnitureForRent.DataAccessLayer;
using FurnitureForRent.Models;
using System.Collections.Generic;

namespace FurnitureForRent.BusinessLayer
{
    public class UserBL
    {
        public UserProfileViewModel ShowProfile(int id)
        {
            UserDA user = new UserDA();
            var profile = user.ShowProfile(id);
            return profile;
        }

        public void AddAddress(AddAddressViewModel addAddressViewModel)
        {
            UserDA user = new UserDA();
            user.AddAddress(addAddressViewModel);

        }
        public AddAddressViewModel ShowAddressById(int id)
        {
            UserDA user = new UserDA();
            var address = user.ShowAddressById(id);
            return address;
        }

        public List<AddAddressViewModel> ShowAllAddress(int userId)
        {
            UserDA user = new UserDA();
            var address = user.ShowAllAddress(userId);
            return address;
        }

        public void EditProfile(UserProfileViewModel userProfileViewModel)
        {
            UserDA user = new UserDA();
            user.EditProfile(userProfileViewModel);
        }
        public void EditAddress(AddAddressViewModel addAddressViewModel)
        {
            UserDA user = new UserDA();
            user.EditAddress(addAddressViewModel);
        }

        public List<OrderAndPaymentViewModel> DisplayOrdersAndPayments(int userId)
        {
            UserDA user = new UserDA();
            var furniture = user.DisplayOrdersAndPayments(userId);
            foreach (var item in furniture)
            {
                if (string.Compare(item.Status, "True", true) == 0)
                {
                    item.Status = "Open";
                }
                else
                {
                    item.Status = "Closed";
                }
            }
            return furniture;
        }

        public List<OrderAndPaymentViewModel> DisplayCart(int userId)
        {
            UserDA user = new UserDA();
            var cart = user.DisplayCart(userId);
            return cart;
        }

        public void PlaceOrder(PlaceOrderViewModel placeOrderViewModel)
        {
            UserDA user = new UserDA();
            user.PlaceOrder(placeOrderViewModel);
        }

        public void AddToCart(AddToCartViewModel addToCartViewModel)
        {
            UserDA user = new UserDA();
            user.AddToCart(addToCartViewModel);
        }

        public void CancelOrder(PlaceOrderViewModel placeOrderViewModel)
        {
            UserDA user = new UserDA();
            user.CancelOrder(placeOrderViewModel);
        }
    }
}