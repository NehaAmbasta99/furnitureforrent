﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName      : AdminBL.cs
//FileType      : Visual C# Source file
//Size          : 6KB
//Author        : Neha Ambasta
//Copy Rights   : FurnitureForRent
//Description   : Class for defining business layer methods for Admin controller for the following things:
//                displaying and editing user profile,Change password,
//                Activate user,deactivate user,update user,
//                display address,add address,update address,
//                Orders And Payments,Furniture Cart,Cancel Order
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.Areas.Admin.Models;
using FurnitureForRent.DataAccessLayer;
using System.Collections.Generic;

namespace FurnitureForRent.BusinessLayer
{
    public class AdminBL
    {
        public AdminProfileViewModel ShowProfile(int id)
        {
            AdminDA admin = new AdminDA();
            var profile = admin.ShowProfile(id);
            return profile;
        }

        public void EditProfile(AdminProfileViewModel adminProfileViewModel)
        {
            AdminDA admin = new AdminDA();
            admin.EditProfile(adminProfileViewModel);
        }

        public List<AllUsersViewModel> ShowAllUsers()
        {
            AdminDA admin = new AdminDA();
            var users = admin.ShowAllUsers();
            foreach (var item in users)
            {
                if (string.Compare(item.Status,"True",true)==0)
                {
                    item.Status = "Active";
                }
                else
                {
                    item.Status = "Inactive";
                }
            }
            return users;
        }

        public void AddFurniture(AddFurnitureViewModel addFurnitureViewModel)
        {
            AdminDA admin = new AdminDA();
            admin.AddFurniture(addFurnitureViewModel);
        }

        public AddFurnitureViewModel GetFurnitureDetails(int id)
        {
            AddFurnitureViewModel homeSearchViewModel = new AddFurnitureViewModel();
            AdminDA home = new AdminDA();
            AddFurnitureViewModel list = home.GetFurnitureDetails(id);
            return list;
        }

        public void UpdateFurniture(AddFurnitureViewModel addFurnitureViewModel)
        {
            AdminDA admin = new AdminDA();
            admin.UpdateFurniture(addFurnitureViewModel);
        }

        //Display All Furnitures available from all category
        public List<DisplayFurnitureViewModel> DisplayAllFurniture()
        {
            AdminDA admin = new AdminDA();
            var furniture = admin.DisplayAllFurniture();
            return furniture;
        }

        public void DeleteFurniture(int id)
        {
            AdminDA admin = new AdminDA();
            admin.DeleteFurniture(id);
        }

        public void ActivateUser(int id)
        {
            AdminDA admin = new AdminDA();
            admin.ActivateUser(id);
        }

        public void DeactivateUser(int id)
        {
            AdminDA admin = new AdminDA();
            admin.DeactivateUser(id);
        }

        public List<DisplayOrdersAndPayments> DisplayOrdersAndPayments()
        {
            AdminDA admin = new AdminDA();
            var furniture = admin.DisplayOrdersAndPayments();
            foreach (var item in furniture)
            {
                if (string.Compare(item.Status,"True",true)==0)
                {
                    item.Status = "Open";
                }
                else
                {
                    item.Status = "Closed";
                }
            }
            return furniture;
        }
        public void AddAddress(AddAddressViewModel addAddressViewModel)
        {
            AdminDA user = new AdminDA();
            user.AddAddress(addAddressViewModel);

        }
        public AddAddressViewModel ShowAddressById(int id)
        {
            AdminDA user = new AdminDA();
            var address = user.ShowAddressById(id);
            return address;
        }

        public List<AddAddressViewModel> ShowAllAddress(int userId)
        {
            AdminDA user = new AdminDA();
            var address = user.ShowAllAddress(userId);
            return address;
        }
        public void EditAddress(AddAddressViewModel addAddressViewModel)
        {
            AdminDA user = new AdminDA();
            user.EditAddress(addAddressViewModel);
        }


    }
}