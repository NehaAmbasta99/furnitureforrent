﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: ChangePasswordBL.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining business layer methods for changing password and converting password into hashed password
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.Areas.Admin.Models;
using FurnitureForRent.DataAccessLayer;
using System.Text;

namespace FurnitureForRent.BusinessLayer
{
    public class ChangePasswordBL
    {
        public int ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            string oldPassword = changePasswordViewModel.OldPassword;
            string newPassword = changePasswordViewModel.NewPassword;
            if (string.Compare(newPassword,changePasswordViewModel.ConfirmPassword,false)==0)
            {
                changePasswordViewModel.OldPasswordHash = sha256(oldPassword);
                changePasswordViewModel.NewPasswordHash = sha256(newPassword);
                ChangePasswordDA registration = new ChangePasswordDA();
                int userExists = registration.ChangePassword(changePasswordViewModel);
                return userExists;
            }
            else return -1;
        }

        private static string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}