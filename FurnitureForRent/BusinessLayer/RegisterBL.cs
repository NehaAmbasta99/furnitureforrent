﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: HomepageBL.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining business layer methods for registering the user,registering an admin,converting password to hash
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.DataAccessLayer;
using FurnitureForRent.Models;
using System.Text;

namespace FurnitureForRent.BusinessLayer
{
    public class RegisterBL
    {
        public int UserRegister(SignUpViewModel signupViewModel)
        {
            string password = signupViewModel.Password;
            signupViewModel.Hash = sha256(password);
            RegisterDA registration = new RegisterDA();
            int userExists = registration.AddUser(signupViewModel);
            return userExists;
        }

        public int AdminRegister(SignUpViewModel signupViewModel)
        {
            string password = signupViewModel.Password;
            signupViewModel.Hash = sha256(password);
            RegisterDA registration = new RegisterDA();
            int userExists = registration.AddAdmin(signupViewModel);
            return userExists;
        }

        private static string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}