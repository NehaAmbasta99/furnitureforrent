﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: HomepageBL.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining business layer methods for ValidateUser,getting user role,converting password into hash
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using FurnitureForRent.DataAccessLayer;
using FurnitureForRent.Models;
using System.Linq;
using System.Text;

namespace FurnitureForRent.BusinessLayer
{
    public class LoginBL
    {
        public int ValidateUser(SignInViewModel signinViewModel)
        {
            if (signinViewModel.UserName.Contains('@'))
            {
                signinViewModel.EmailId = signinViewModel.UserName;
            }
            else
            {
                signinViewModel.MobileNumber = signinViewModel.UserName;
            }
            string password = signinViewModel.Password;
            signinViewModel.Hash = sha256(password);
            LoginDA login = new LoginDA();
            int userId = login.ValidateLogin(signinViewModel);
            return userId;
        }

        private static string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public int GetUserRole(int UserId)
        {
            LoginDA login = new LoginDA();
            int roleId = login.GetRole(UserId);
            return roleId;
        }
    }
}