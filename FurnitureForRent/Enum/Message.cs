﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: Message.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining constants for errors DataAlreadyAvailable,DataSavedSuccessfully,EnterDataInAllFields,IncorrectData,PleaseSignIn
//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace FurnitureForRent.Enum
{
    public static class Message
    {
        public  const string DataAlreadyAvailable = "User Exists with the same data kindly recheck your data or register with a different data!";
        public  const string DataSavedSuccessfully = "Data has been saved successfully!";
        public  const string EnterDataInAllFields = "Enter data in all fields!";
        public  const string IncorrectData = "Entered data is incorrect!";
        public  const string PleaseSignIn = "Please Sign In!";
        public const string AddedToCart = "Item added to cart!";
        public const string OrderPlaced = "Your order has been placed!";
        public const string PasswordChanged = "Your password has been changed recently!";
        public const string OrderCancelled = "Your order has been cancelled!";
        public const string Notification = "FurnitureForRent:Notification";
        public const string Confirmation = "FurnitureForRent:Confirmation";
        public const string Cancellation = "FurnitureForRent:Cancellation";
    }
}