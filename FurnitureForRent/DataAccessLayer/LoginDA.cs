﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: LoginDA.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining data access layer methods for validating a user for login and getting the role from database
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using Dapper;
using FurnitureForRent.Models;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FurnitureForRent.DataAccessLayer
{
    public class LoginDA
    {
        private DataContext dataContext = new DataContext();

        public int ValidateLogin(SignInViewModel signin)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var loginParam = new { @MobileNumber = signin.MobileNumber, @EmailId = signin.EmailId, @Password = signin.Hash };
                int userId = connection.Query<int>("MainLoginCheck", loginParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return userId;
            }
        }

        public int GetRole(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var roleParam = new { @UserId = userId };
                int roleId = connection.Query<int>("MainGetUserRole", roleParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return roleId;
            }
        }
    }
}