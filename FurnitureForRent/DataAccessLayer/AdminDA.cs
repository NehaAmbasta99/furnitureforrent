﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName      : AdminBL.cs
//FileType      : Visual C# Source file
//Size          : 6KB
//Author        : Neha Ambasta
//Copy Rights   : FurnitureForRent
//Description   : Class for defining data access layer methods for Admin controller for the following things:
//                displaying and editing user profile,Change password,
//                Activate user,deactivate user,update user,
//                display address,add address,update address,
//                Orders And Payments,Furniture Cart,Cancel Order
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using Dapper;
using FurnitureForRent.Areas.Admin.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FurnitureForRent.DataAccessLayer
{
    public class AdminDA
    {
        public AdminProfileViewModel ShowProfile(int userId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var adminParam = new
                {
                    @UserId = userId
                };
                AdminProfileViewModel admin = connection.Query<AdminProfileViewModel>("UserDisplayProfile", adminParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return admin;
            }
        }

        public void EditProfile(AdminProfileViewModel adminProfileViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var adminParam = new
                {
                    @UserId = adminProfileViewModel.UserId,
                    @FullName = adminProfileViewModel.FullName,
                    @MobileNumber = adminProfileViewModel.MobileNumber
                };
                connection.Execute("UserEditProfile", adminParam, commandType: CommandType.StoredProcedure);
            }
        }

        public List<AllUsersViewModel> ShowAllUsers()
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var users = connection.Query<AllUsersViewModel>("AdminDisplayAllUser", commandType: CommandType.StoredProcedure).ToList();
                return users;
            }
        }

        public void AddFurniture(AddFurnitureViewModel addFurnitureViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureParam = new
                {
                    @FurnitureCode = addFurnitureViewModel.FurnitureCode,
                    @Category = addFurnitureViewModel.Category,
                    @Color = addFurnitureViewModel.Color,
                    @Dimension = addFurnitureViewModel.Dimension,
                    @Material = addFurnitureViewModel.Material,
                    @RefundableDeposit = addFurnitureViewModel.RefundableDeposit,
                    @RentPerMonth = addFurnitureViewModel.RentPerMonth,
                    @Specification = addFurnitureViewModel.Specification
                };
                connection.Execute("AdminAddFurniture", furnitureParam, commandType: CommandType.StoredProcedure);
            }
        }

        public AddFurnitureViewModel GetFurnitureDetails(int id)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureParam = new { @FurnitureId = id };
                AddFurnitureViewModel furnitureList = connection.Query<AddFurnitureViewModel>("MainFurnitureDetails", furnitureParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return furnitureList;
            }
        }

        public void UpdateFurniture(AddFurnitureViewModel addFurnitureViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureParam = new
                {
                    @FurnitureCode = addFurnitureViewModel.FurnitureCode,
                    @FurnitureId = addFurnitureViewModel.FurnitureId,
                    @Category = addFurnitureViewModel.Category,
                    @Color = addFurnitureViewModel.Color,
                    @Dimension = addFurnitureViewModel.Dimension,
                    @Material = addFurnitureViewModel.Material,
                    @RefundableDeposit = addFurnitureViewModel.RefundableDeposit,
                    @RentPerMonth = addFurnitureViewModel.RentPerMonth,
                    @Specification = addFurnitureViewModel.Specification
                };
                connection.Execute("AdminEditFurniture", furnitureParam, commandType: CommandType.StoredProcedure);
            }
        }

        public List<DisplayFurnitureViewModel> DisplayAllFurniture()
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furniture = connection.Query<DisplayFurnitureViewModel>("MainDisplayAllFurniture", commandType: CommandType.StoredProcedure).ToList();
                return furniture;
            }
        }

        public void DeleteFurniture(int id)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureParam = new
                {
                    @FurnitureId = id
                };
                connection.Execute("AdminDeleteFurniture", furnitureParam, commandType: CommandType.StoredProcedure);
            }
        }

        public void ActivateUser(int id)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var activateParam = new
                {
                    @UserId = id
                };
                connection.Execute("AdminActivateUser", activateParam, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeactivateUser(int id)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var deactivateParam = new
                {
                    @UserId = id
                };
                connection.Execute("AdminDeactivateUser", deactivateParam, commandType: CommandType.StoredProcedure);
            }
        }

        public List<DisplayOrdersAndPayments> DisplayOrdersAndPayments()
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var order = connection.Query<DisplayOrdersAndPayments>("AdminDisplayOrder", commandType: CommandType.StoredProcedure).ToList();
                return order;
            }
        }
        public void EditAddress(AddAddressViewModel addAddressViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var addressParam = new
                {
                    @FlatOrHouseNumber = addAddressViewModel.FlatOrHouseNumber,
                    @Landmark = addAddressViewModel.Landmark,
                    @Pincode = addAddressViewModel.Pincode,
                    @SocietyName = addAddressViewModel.SocietyName,
                    @UserId = addAddressViewModel.UserId,
                    @Address1 = addAddressViewModel.Address1,
                    @Address2 = addAddressViewModel.Address2,
                    @AlternativeMobileNumber = addAddressViewModel.AlternativeMobileNumber,
                    @Area = addAddressViewModel.Area,
                    @CityId = addAddressViewModel.CityId,
                    @AddressId = addAddressViewModel.AddressId
                };
                connection.Execute("UserEditAddress", addressParam, commandType: CommandType.StoredProcedure);
            }
        }
        public AddAddressViewModel ShowAddressById(int addressId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @AddressId = addressId
                };
                AddAddressViewModel user = connection.Query<AddAddressViewModel>("UserDisplayAddress", userParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return user;
            }
        }

        public List<AddAddressViewModel> ShowAllAddress(int userId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @UserId = userId
                };
                List<AddAddressViewModel> user = connection.Query<AddAddressViewModel>("UserDisplayAllAddress", userParam, commandType: CommandType.StoredProcedure).ToList();
                return user;
            }
        }
        public void AddAddress(AddAddressViewModel addAddressViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @FlatOrHouseNumber = addAddressViewModel.FlatOrHouseNumber,
                    @Landmark = addAddressViewModel.Landmark,
                    @Pincode = addAddressViewModel.Pincode,
                    @SocietyName = addAddressViewModel.SocietyName,
                    @UserId = addAddressViewModel.UserId,
                    @Address1 = addAddressViewModel.Address1,
                    @Address2 = addAddressViewModel.Address2,
                    @AlternativeMobileNumber = addAddressViewModel.AlternativeMobileNumber,
                    @Area = addAddressViewModel.Area,
                    @CityId = addAddressViewModel.CityId
                };
                connection.Execute("UserAddAddress", userParam, commandType: CommandType.StoredProcedure);
            }
        }

    }
}