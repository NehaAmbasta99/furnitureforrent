﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: DataContext.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining data access layer methods for defining the connection with the database using connection string
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.Configuration;
using System.Data.SqlClient;

namespace FurnitureForRent.DataAccessLayer
{
    public class DataContext
    {
        public SqlConnection connection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}