﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: HomepageDA.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining data access layer methods for Getting Furniture list for dropdown on homepage
//              Get Furniture Details , display All Furniture
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using Dapper;
using FurnitureForRent.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FurnitureForRent.DataAccessLayer
{
    public class HomepageDA : DataContext
    {
        private DataContext dataContext = new DataContext();

        public IEnumerable<HomeSearchViewModel> GetFurnitureList()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureList = connection.Query<HomeSearchViewModel>("MainDisplayAllFurniture", commandType: CommandType.StoredProcedure).ToList();
                return furnitureList;
            }
        }

        public List<HomeSearchViewModel> GetFurnitureDetails(HomeSearchViewModel search)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureParam = new { @FurnitureId = search.FurnitureId, @FurnitureCode = search.FurnitureCode };
                List<HomeSearchViewModel> furnitureList = connection.Query<HomeSearchViewModel>("MainFurnitureDetails", furnitureParam, commandType: CommandType.StoredProcedure).ToList();
                return furnitureList;
            }
        }

        public List<HomeSearchViewModel> DisplayAllFurniture()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var furnitureList = connection.Query<HomeSearchViewModel>("MainDisplayAllFurniture", commandType: CommandType.StoredProcedure).ToList();
                return furnitureList;
            }
        }
    }
}