﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: UserDA.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining data access layer methods for displaying profile,adding address,displaying a address by its id,displaying  all address,
//              edit profile,edit address,displaying orders and payments,placing an order,cancel an order,add to cart,display cart
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using Dapper;
using FurnitureForRent.Areas.User.Models;
using FurnitureForRent.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FurnitureForRent.DataAccessLayer
{
    public class UserDA
    {
        public UserProfileViewModel ShowProfile(int userId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @UserId = userId
                };
                UserProfileViewModel user = connection.Query<UserProfileViewModel>("UserDisplayProfile", userParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return user;
            }
        }

        public void EditProfile(UserProfileViewModel userProfileViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @UserId = userProfileViewModel.UserId,
                    @FullName = userProfileViewModel.FullName,
                    @MobileNumber = userProfileViewModel.MobileNumber
                };
                connection.Execute("UserEditProfile", userParam, commandType: CommandType.StoredProcedure);
            }
        }
        public void EditAddress(AddAddressViewModel addAddressViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var addressParam = new
                {
                    @FlatOrHouseNumber = addAddressViewModel.FlatOrHouseNumber,
                    @Landmark = addAddressViewModel.Landmark,
                    @Pincode = addAddressViewModel.Pincode,
                    @SocietyName = addAddressViewModel.SocietyName,
                    @UserId = addAddressViewModel.UserId,
                    @Address1 = addAddressViewModel.Address1,
                    @Address2 = addAddressViewModel.Address2,
                    @AlternativeMobileNumber = addAddressViewModel.AlternativeMobileNumber,
                    @Area = addAddressViewModel.Area,
                    @CityId = addAddressViewModel.CityId,
                    @AddressId = addAddressViewModel.AddressId
                };
                connection.Execute("UserEditAddress", addressParam, commandType: CommandType.StoredProcedure);
            }
        }

        public AddAddressViewModel ShowAddressById(int addressId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @AddressId = addressId
                };
                AddAddressViewModel user = connection.Query<AddAddressViewModel>("UserDisplayAddress", userParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return user;
            }
        }

        public List<AddAddressViewModel> ShowAllAddress(int userId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @UserId = userId
                };
                List<AddAddressViewModel> user = connection.Query<AddAddressViewModel>("UserDisplayAllAddress", userParam, commandType: CommandType.StoredProcedure).ToList();
                return user;
            }
        }
        public void AddAddress(AddAddressViewModel addAddressViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var userParam = new
                {
                    @FlatOrHouseNumber = addAddressViewModel.FlatOrHouseNumber,
                    @Landmark = addAddressViewModel.Landmark,
                    @Pincode = addAddressViewModel.Pincode,
                    @SocietyName = addAddressViewModel.SocietyName,
                    @UserId = addAddressViewModel.UserId,
                    @Address1 = addAddressViewModel.Address1,
                    @Address2 = addAddressViewModel.Address2,
                    @AlternativeMobileNumber = addAddressViewModel.AlternativeMobileNumber,
                    @Area = addAddressViewModel.Area,
                    @CityId = addAddressViewModel.CityId
                };
                connection.Execute("UserAddAddress", userParam, commandType: CommandType.StoredProcedure);
            }
        }

        public void PlaceOrder(PlaceOrderViewModel placeOrderViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var orderParam = new
                {
                    @UserId = placeOrderViewModel.UserId,
                    @Quantity = placeOrderViewModel.Quantity,
                    @Tenure = placeOrderViewModel.Tenure,
                    @AddressId = placeOrderViewModel.AddressId,
                    @FurnitureId = placeOrderViewModel.FurnitureId,
                    @RentalDue = placeOrderViewModel.RentalDue,
                    @Adjustement = placeOrderViewModel.Adjustment,
                    @CartId = placeOrderViewModel.CartId
                };
                connection.Execute("UserPlaceOrder", orderParam, commandType: CommandType.StoredProcedure);
            }
        }

        public void AddToCart(AddToCartViewModel addToCartViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var cartParam = new
                {
                    @UserId = addToCartViewModel.UserId,
                    @FurnitureId = addToCartViewModel.FurnitureId,
                    @Tenure = addToCartViewModel.Tenure
                };
                connection.Execute("UserAddToCart", cartParam, commandType: CommandType.StoredProcedure);
            }
        }

        public List<OrderAndPaymentViewModel> DisplayOrdersAndPayments(int userId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var orderParam = new
                {
                    @UserId = userId
                };
                var order = connection.Query<OrderAndPaymentViewModel>("UserDisplayOrder", orderParam, commandType: CommandType.StoredProcedure).ToList();
                return order;
            }
        }

        public List<OrderAndPaymentViewModel> DisplayCart(int userId)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var orderParam = new
                {
                    @UserId = userId
                };
                var order = connection.Query<OrderAndPaymentViewModel>("UserDisplayCart", orderParam, commandType: CommandType.StoredProcedure).ToList();
                return order;
            }
        }

        public void CancelOrder(PlaceOrderViewModel placeOrderViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var orderParam = new
                {
                    @UserId = placeOrderViewModel.UserId,
                    @OrderId = placeOrderViewModel.OrderId
                };
                connection.Execute("UserCancelOrder", orderParam, commandType: CommandType.StoredProcedure);
            }
        }
    }
}