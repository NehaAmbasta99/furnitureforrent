﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: RegisterDA.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining data access layer methods for registering the user,registering an admin 
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using Dapper;
using FurnitureForRent.Models;
using System.Data;
using System.Data.SqlClient;

namespace FurnitureForRent.DataAccessLayer
{
    public class RegisterDA
    {
        public int AddUser(SignUpViewModel signupViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var registerParam = new
                {
                    @FullName = signupViewModel.FullName,
                    @MobileNumber = signupViewModel.MobileNumber,
                    @EmailId = signupViewModel.EmailId,
                    @Password = signupViewModel.Hash
                };
                var userExists = connection.Execute("MainUserAddDetail", registerParam, commandType: CommandType.StoredProcedure);
                return userExists;
            }
        }

        public int AddAdmin(SignUpViewModel signupViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var registerParam = new
                {
                    @FullName = signupViewModel.FullName,
                    @MobileNumber = signupViewModel.MobileNumber,
                    @EmailId = signupViewModel.EmailId,
                    @Password = signupViewModel.Hash
                };
                var userExists = connection.Execute("AdminAddAdminDetail", registerParam, commandType: CommandType.StoredProcedure);
                return userExists;
            }
        }
    }
}