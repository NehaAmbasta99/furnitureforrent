﻿////////////////////////////////////////////////////////////////////////////////////////////////////////
//FileName: ChangePasswordDA.cs
//FileType: Visual C# Source file
//Size : 6KB
//Author : Neha Ambasta
//Copy Rights : FurnitureForRent
//Description : Class for defining data access layer methods for changing password and converting password into hashed password
//////////////////////////////////////////////////////////////////////////////////////////////////////////
using Dapper;
using FurnitureForRent.Areas.Admin.Models;
using System.Data;
using System.Data.SqlClient;

namespace FurnitureForRent.DataAccessLayer
{
    public class ChangePasswordDA
    {
        public int ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            DataContext dataContext = new DataContext();
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var registerParam = new
                {
                    @UserId = changePasswordViewModel.UserId,
                    @OldPassword = changePasswordViewModel.OldPasswordHash,
                    @NewPassword = changePasswordViewModel.NewPasswordHash
                };
                var userExists = connection.Execute("UserChangePassword", registerParam, commandType: CommandType.StoredProcedure);
                return userExists;
            }
        }
    }
}